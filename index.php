<?php

    $hostname = 'localhost';
    $username = 'root';
    $password = '';
    $database = 'college';

    function connectToDB($hostname, $username, $password, $database) {
        $db = new mysqli($hostname, $username, $password, $database);

        if ($db->connect_errno) {
            echo 'Failed to connect to database' . $db->connect_errno;
            exit();
        }

        return $db;
    }

    function insertStudents($db, $studentName, $studentEmail) {
        $query = "INSERT INTO students(name, email) VALUES (?, ?)";

        $statement = $db->prepare($query);
        $statement->bind_param('ss', $studentName, $studentEmail);

        $statement->execute();
    }

    function selectStudentIDs($db) {
        $query = "SELECT student_id from enrollments";
        $statement = $db->prepare($query);
        $statement->execute();
        $queryResult = $statement->get_result();

        return $queryResult->fetch_all(MYSQLI_ASSOC);
    }

    function insertEnrollment($db, $enrollment) {
        $query = "INSERT INTO enrollments(student_id) VALUES (?)";

        $statement = $db->prepare($query);
        $statement->bind_param('i', $enrollment);

        $statement->execute();
    }

    function deleteEnrollment($db, $enrollment) {
        $query = "DELETE FROM enrollments WHERE student_id = ?";

        $statement = $db->prepare($query);
        $statement->bind_param('i', $enrollment);

        $statement->execute();
    }

    function deleteAllEnrollments($db) {
        $query = "DELETE FROM enrollments";

        $statement = $db->prepare($query);

        $statement->execute();
    }


    $db = connectToDB($hostname, $username, $password, $database);


    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if (isset($_POST['name']) && isset($_POST['email'])) {
            $studentName = $_POST['name'];
            $studentEmail = $_POST['email'];

            insertStudents($db, $studentName, $studentEmail);
            header("Refresh:0");
            exit();
        }

        $currentEnrollments = selectStudentIDs($db);
        $currentStudentIDs = array_column($currentEnrollments, 'student_id');

        if (isset($_POST['enrollments'])) {
            $enrollments = $_POST['enrollments'];

            $uncheckedElements = array_diff($currentStudentIDs, $enrollments);

            foreach ($uncheckedElements as $element) {
                deleteEnrollment($db, $element);
            }

            foreach ($enrollments as $enrollment) {
                if (!in_array($enrollment, $currentStudentIDs)) {
                    insertEnrollment($db, $enrollment);
                }
            }
        } else {
            deleteAllEnrollments($db);
        }
    }

    $querySelectStudents = "SELECT * FROM students";
    $statement = $db->prepare($querySelectStudents);
    $statement->execute();
    $queryResult = $statement->get_result();

    $receivedStudents = $queryResult->fetch_all(MYSQLI_ASSOC);

    $currentEnrollments = selectStudentIDs($db);

    $currentStudentIDs = array_column($currentEnrollments, 'student_id');

    ?>

    <form action="" method="POST">
        <label id="name" for="name">Имя: </label>
        <input type="text" name="name">
        <label id="email" for="email">Email: </label>
        <input type="email" name="email">
        <button type="submit">Добавить в БД</button>
    </form>

    <form action="" method="POST">
        <?php foreach($receivedStudents as $student) : ?>
        <label id="name"
               for="name"><?php echo $student['name'] ?></label>
        <input id="student<?php echo $student['id']?>"
               type="checkbox"
               name="enrollments[]"
               value="<?php echo $student['id']?>"
            <?php if (in_array($student['id'], $currentStudentIDs)) {echo 'checked = "checked"';} ?>/>
        <br>
        <?php endforeach; ?>
        <button type="submit">Обновить БД</button>
    </form>